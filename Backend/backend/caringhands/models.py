from django.db import models
from django.contrib.postgres.fields import ArrayField
from django.db import connection, transaction
import json


DICT = {"Alaska": "ak",
        "Alabama": "al",
        "Arkansas": "ar",
        "Arizona": "az",
        "California": "ca",
        "Colorado": "co",
        "Connecticut": "ct",
        "District_of_Columbia": "dc",
        "Delaware": "de",
        "Florida": "fl",
        "Georgia": "ga",
        "Hawaii": "hi",
        "Iowa": "ia",
        "Idaho": "id",
        "Illinois": "il",
        "Indiana": "in",
        "Kansas": "ks",
        "Kentucky": "ky",
        "Louisiana": "la",
        "Massachusetts": "ma",
        "Maryland": "md",
        "Maine": "me",
        "Michigan": "mi",
        "Minnesota": "mn",
        "Missouri": "mo",
        "Mississippi": "ms",
        "Montana": "mt",
        "North_Carolina": "nc",
        "North_Dakota": "nd",
        "Nebraska": "ne",
        "New_Hampshire": "nh",
        "New_Jersey": "nj",
        "New_Mexico": "nm",
        "Nevada": "nv",
        "New_York": "ny",
        "Ohio": "oh",
        "Oklahoma": "ok",
        "Oregon": "or",
        "Pennsylvania": "pa",
        "Rhode_Island": "ri",
        "South_Carolina": "sc",
        "South_Dakota": "sd",
        "Tennessee": "tn",
        "Texas": "tx",
        "Utah": "ut",
        "Virginia": "va",
        "Vermont": "vt",
        "Washington": "wa",
        "Wisconsin": "wi",
        "West_Virginia": "wv",
        "Wyoming": "wy"
        }
INV_DICT = {v: k for k, v in DICT.items()}


class States(models.Model):
    name = models.CharField(max_length=25, default="", primary_key=True)
    poverty = models.CharField(max_length=5, default=0)
    abrv = models.CharField(max_length=25, default="")
    region = models.CharField(max_length=25, default="")
    description = models.TextField(default="")
    pos_img = models.URLField(default="")
    main_img = models.URLField(default="")

    class Meta:
        ordering = ('name',)

    def __str__(self):
        return self.name


class Cities(models.Model):
    city = models.CharField(max_length=40, default="", primary_key=True)
    abrv = models.ForeignKey(States, on_delete=models.CASCADE)


    class Meta:
        ordering = ('city',)

    def __str__(self):
        return self.city


class Volunteering(models.Model):
    name = models.CharField(max_length=50, default="")
    description = models.TextField(default="")
    city = models.CharField(max_length=25, default="")
    abrv = models.CharField(max_length=25, default="")
    address = models.TextField(default="")
    main_img = models.URLField(default="")
    logo_img = models.URLField(default="")
    website = models.URLField(default="")
    id = models.IntegerField(default=0, primary_key=True)

    @property
    def imgSrc(self):
        return self.logo_img

    @property
    def state(self):
        CURSOR = connection.cursor()
        CURSOR.execute(
            "SELECT caringhands_states.name,caringhands_states.poverty FROM caringhands_states WHERE caringhands_states.Abrv = '" + self.abrv + "';")
        json = CURSOR.fetchone()
        state = [str(json[0]),str(json[1])]
        return state


    class Meta:
        ordering = ('id',)


class Pantries(models.Model):
    name = models.CharField(max_length=50, default="")
    abrv = models.CharField(max_length=5, default="")
    description = models.TextField(default="")
    address = models.TextField(default="")
    city = models.CharField(max_length=40, default="")
    logo_img = models.URLField(default="")
    website = models.URLField(default="")
    id = models.IntegerField(default=0, primary_key=True)

    @property
    def state_pos_img(self):
        CURSOR = connection.cursor()
        CURSOR.execute(
            "SELECT caringhands_states.pos_img FROM caringhands_states WHERE caringhands_states.Abrv = '" + self.abrv + "';")
        json = CURSOR.fetchone()
        state_pos_img = str(json[0])
        return state_pos_img

    @property
    def imgSrc(self):
        return self.logo_img

    @property
    def state(self):
        CURSOR = connection.cursor()
        CURSOR.execute(
            "SELECT caringhands_states.name, caringhands_states.poverty FROM caringhands_states INNER JOIN caringhands_pantries ON caringhands_pantries.Abrv = '" +
            self.abrv +
            "' AND caringhands_states.Abrv = '" +
            self.abrv +
            "';")
        json = CURSOR.fetchone()
        state = [str(json[0]),str(json[1])]

        return state

    class Meta:
        ordering = ('id',)

    def __str__(self):
        return self.name

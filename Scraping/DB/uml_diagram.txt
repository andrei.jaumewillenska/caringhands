@startuml
State "1" -- "many" City
City "1" *-- "many" CharityOpportunity
City "1" *-- "many" FoodPantries

State "1" -- "0-N" CharityOpportunity
State "1" -- "0-N" FoodPantries

class State {
   + String name	
   + String Abrv
   + String poverty_rate
   + String description
   + String main_img
   + String pos_img
   + String flag_img
   + primary_key (name): string
}

class City {
   + String name
   + String Abrv
   + primary_key (name,abrv): string
   + foreign_key (state): abrv
}

class CharityOpportunity {
   + string name
   + string website
   + string city
   + string address
   + string description
   + string abrv
   + string main_img
   + string logo_img
   + string state_pos_img
   + primarykey (name,city): string
   + foreign key (cities) : (city,abrv)
}

class FoodPantries{
  + string name
  + string abrv
  + string description
  + string address
  + string city
  + string website
  + string logo_img
  + primary_key (name,city): string
  + foreign key (cities) : (city,abrv)
}
@enduml

# CaringHands

Name: CaringHands, caringhands.me
Goal: Strong focus on civic engagement through allowing individuals the opportunity to match with an organization that will allow them to donate their own time and resources to help those in need primarily through the provision of basic necessitates such as food and volunteering.
Datasets: Wikipedia, foodpantries.org, volunteermatch.org
States in the US, food pantries within US states, hands-on volunteering opportunities within states
State data will contain homelessness statistics, data on large organizations dedicated to helping those in need, information on any legislation that was made to alleviate poverty
Food pantries within the US by state, link to information on what services they provide, address phone number to get in contact with them
volunteering opportunities will provide listings of local opportunities, their specifications, and their potential community impact

http://caringhands.me/

CI:

Pipeline for backend:
https://gitlab.com/andrei.jaumewillenska/caringhands/pipelines

Pipeline for frontend:
https://gitlab.com/Kiwami/caringhands-frontend/pipelines


Team Members:

 -  Andrei Jaume
    EID: aj26363
    Gitlab: andrei.jaumewillenska
    Estiamted Time of Completion for Phase 3: 15 hours
    Actual Time of Completion for Phase 3: 30 hours


 -  Lingfei He
    EID: lh33566
    Gitlab: HeliumHeLingfei
    Estiamted Time of Completion for Phase 3: 15 hours
    Actual Time of Completion for Phase 3: 30 hours

 -  Julia Hoang
    EID: jh63227
    Gitlab: juliahoang
    Estimated Time of Completion for Phase 3: 15 hours
    Actual Time of Completion for Phase 3: 30 hours

-  Sergio Moreno
    EID: sm62588
    Gitlab: kiwami
    Estimated Time of Completion for Phase 3: 17 hours
    Actual Time of Completion for Phase 3: 30 hours

-   Xi Jin
    EID: xj924
    Gitlab: jinxi97
    Estimated Time of Completion for Phase 3: 10 hours
    Actual Time of Completion for Phase 3: 30 hours


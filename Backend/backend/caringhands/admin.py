from django.contrib import admin
from . models import States, Cities,Pantries

admin.site.register(States)
admin.site.register(Pantries)
admin.site.register(Cities)

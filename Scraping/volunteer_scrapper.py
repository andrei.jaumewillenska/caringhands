# Scrapping tool for the volunteering opportunities
# Author Andrei Jaume

import requests
import os
import re
from os import listdir
from os.path import isfile, join
from food_pantry_city import get_dict

URL  = "https://www.givepulse.com/search?SearchForm[location]="

# Function to clean the text, and make it more readable
def clean_text(raw_text):
    clean_text = raw_text
    clean_text = os.linesep.join([s for s in clean_text.splitlines() if s])
    clean_text = re.sub("\"", "", clean_text)
    clean_text = re.sub("'", "", clean_text)
    return clean_text

# Function to remove html tags
def remove_html(raw_text):
    clean_comp = re.compile("<.*?>")
    clean_text = re.sub(clean_comp, "\n", raw_text)
    clean_text = re.sub("&nbsp;", "", clean_text)
    return clean_text

# Function to get the full text description, location and header image
def get_page_info(url):
    try:        
        data = requests.get(url)
        result = clean_text(data.text)
        text = remove_html(result)
                
        header_image = re.search("background-image: url\((.*)\)", result).group(1).strip()
        location = ""
        google_image = ""
        
        try:
            location = re.search("address_1>(.*) </span>", result).group(1).strip()
        except AttributeError:
            pass
        finally:
            location += " " + re.search("address_3>(.*)</span>", result).group(1).strip()
            location = remove_html(location).replace("\n", "")
            
        google_image = re.search("<img id=event-map style=width: 100%; src=(.*) />", result).group(1).strip()
        description = ""
        try:
            description = remove_html(re.search("describe-event>(.*)</div>", result).group(1).strip())
        except AttributeError:
        
            keep = False
            for line in text.split("\n"):
                if keep and "GivePulse" in line:
                    break
                if keep:
                    description += line
                if "Description" in line:
                    keep = True
                    
        return description, header_image, location, google_image
    except AttributeError as e:
        print("bad info on ", url)
        return None,None,None,None
    except:
        print("caught some exception")
        return None,None,None,None

# Function to query the api for a specific state
def query_volunteer(sql_string, city, abrv, dest):
    
    # Search by city and state    
    data = requests.get(URL + city.replace("_","+") + ",+" + abrv)
    result = clean_text(data.text).split("\n")
    web_base = "https://www.givepulse.com"

    keep = False
    instance_finished = False
    name = ""
    info = ""
    logo_image = ""
    website = ""
    times = 0

        
    # Parse the output
    for line in result:
        if "item-link event-list-mobile" in line:
            keep = True
            instance_finished = False
            website = web_base + re.search("href=(.*)>", line).group(1).strip()
        if keep and "div> <!-- End .info -->" in line:
            keep = False
            instance_finished = True
        if keep and "title=" in line:
            name = re.search("title=(.*)href", line).group(1).strip()
            name = name.split(",")[0]
        if keep and "div class=thumbnail-img" in line:
            logo_image = re.search("\((.*)\)", line).group(1).strip()

        if instance_finished:
            keep = False
            instance_finished = False
            info, main_image, location, google_image = get_page_info(website)

            if location is not None:
                temp_abrv = ""
                try:
                    temp_abrv = (location.split(",")[2]).split(" ")[1]
                    temp_abrv = temp_abrv.lower()
                except IndexError:
                    temp_abrv = (location.split(",")[1]).split(" ")[1]
                    temp_abrv = temp_abrv.lower()
                if temp_abrv != abrv:
                    return
            
            if info is not None and main_image is not None and location is not None and google_image is not None:
            
                output = sql_string + "'" + name + "', '" + info  + "', '" + city\
                         + "', '" + abrv + "', '" + location + "', '" + website \
                         + "', '" + logo_image + "', '" + main_image + "', '" +\
                         google_image + "');"
                fi = open(dest, "a+")
                fi.write(output)
                fi.close()
                times += 1
                
        if times > 9:
            return
                         
# Main function to run the volunteer opporunities scrapper
def main():
    print('Scrapping the volunteering opportunities')
    states_dict = get_dict()
    cities = "./states/cities/"
    files = [f for f in listdir(cities) if isfile(join(cities,f))]
    files.remove(".gitkeep")
    
    dest = "./DB/volunteer.sql"
    sql_string = "INSERT INTO VOLUNTEER (Name, Description, City, Abrv, Address, Website, logo_img, main_img, state_pos_img) VALUES ("

    for f in files:
        file_path = cities+f
        abrv = states_dict.get(f.split("-")[0])

        print("state is", abrv)

        with open(file_path) as fp:
            city = fp.readline().strip()
            
            while city:
                print("city is", city)
                query_volunteer(sql_string, city, abrv, dest)
                city = fp.readline().strip()
                
if __name__ == "__main__":
    main()

# Generated by Django 2.1.2 on 2018-10-26 05:10

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('caringhands', '0002_auto_20181026_0009'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='cities',
            options={'ordering': ('city',)},
        ),
        migrations.AlterModelOptions(
            name='pantries',
            options={'ordering': ('pantry',)},
        ),
        migrations.AlterModelOptions(
            name='states',
            options={'ordering': ('state_name',)},
        ),
        migrations.RemoveField(
            model_name='cities',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='pantries',
            name='created_at',
        ),
        migrations.RemoveField(
            model_name='states',
            name='created_at',
        ),
    ]

# -*- coding: UTF-8 -*-
from rest_framework import serializers
from caringhands.models import States, Pantries, Cities, Volunteering
from django.db import connection, transaction

PANTRY_IMAGE = 50


class StatesSerializer(serializers.ModelSerializer):
    class Meta:
        model = States
        fields = '__all__'

class VisualizationSerializer(serializers.ModelSerializer):
    volunteer=serializers.SerializerMethodField()
    foodpantry=serializers.SerializerMethodField()
    class Meta:
        model = States
        fields = ['name','abrv','volunteer','foodpantry']

    def get_foodpantry(self,obj):
        CURSOR = connection.cursor()
        CURSOR.execute(
            "SELECT count(*) FROM caringhands_pantries INNER JOIN caringhands_states ON caringhands_states.Abrv = caringhands_pantries.Abrv AND caringhands_states.name = '" +
            obj.name +
            "';")
        json = CURSOR.fetchall()
        return json[0][0]

    def get_volunteer(self,obj):
        CURSOR = connection.cursor()
        CURSOR.execute(
            "SELECT count(*) FROM caringhands_volunteering INNER JOIN caringhands_states ON caringhands_states.Abrv = caringhands_volunteering.Abrv AND caringhands_states.name = '" +
            obj.name +
            "';")
        json = CURSOR.fetchall()
        return json[0][0]


class CitiesSerializer(serializers.ModelSerializer):
    class Meta:
        model = Cities
        fields = '__all__'


class PantriesSerializer(serializers.ModelSerializer):
    state = serializers.ListField(required=True)
    state_pos_img = serializers.URLField(required=True)
    main_img = serializers.SerializerMethodField()

    class Meta:
        model = Pantries
        fields = '__all__'

    def get_main_img(self, obj):
        CURSOR = connection.cursor()
        CURSOR.execute("SELECT * FROM caringhands_pantry_images")
        json = CURSOR.fetchall()
        main_img = str(json[obj.id % PANTRY_IMAGE][0])
        return main_img


class VolunteeringSerializer(serializers.ModelSerializer):
    state = serializers.ListField(required=True)
    state_pos_img = serializers.SerializerMethodField()

    class Meta:
        model = Volunteering
        fields = '__all__'

    def get_state_pos_img(self, obj):
        CURSOR = connection.cursor()
        CURSOR.execute(
            "SELECT caringhands_states.pos_img FROM caringhands_states INNER JOIN caringhands_volunteering ON caringhands_volunteering.Abrv = '" +
            obj.abrv +
            "' AND caringhands_states.Abrv = '" +
            obj.abrv +
            "';")
        json = CURSOR.fetchone()
        state_pos_img = str(json[0])
        return state_pos_img


class MiniPantriesSerializer(serializers.ModelSerializer):
    imgSrc = serializers.CharField(required=True)

    class Meta:
        model = Pantries
        fields = ['imgSrc', 'name', 'id']


class MiniVolunteeringSerializer(serializers.ModelSerializer):
    imgSrc = serializers.CharField(required=True)

    class Meta:
        model = Volunteering
        fields = ['imgSrc', 'name', 'id']

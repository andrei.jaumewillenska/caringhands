# Python file to scrape the food pantries by city
# Author Andrei Jaume

import requests
import re
import os
from os import listdir
from os.path import isfile, join

import urllib.request
import urllib.parse

# State to abrv mapping
DICT = { "Alaska":"ak",
         "Alabama":"al",
         "Arkansas":"ar",
         "Arizona":"az",
         "California":"ca",
         "Colorado":"co",
         "Connecticut":"ct",
         "District_of_Columbia":"dc",
         "Delaware":"de",
         "Florida":"fl",
         "Georgia":"ga",
         "Hawaii":"hi",
         "Iowa":"ia",
         "Idaho":"id",
         "Illinois":"il",
         "Indiana":"in",
         "Kansas":"ks",
         "Kentucky":"ky",
         "Louisiana":"la",
         "Massachusetts":"ma",
         "Maryland":"md",
         "Maine":"me",
         "Michigan":"mi",
         "Minnesota":"mn",
         "Missouri":"mo",
         "Mississippi":"ms",
         "Montana":"mt",
         "North_Carolina":"nc",
         "North_Dakota":"nd",
         "Nebraska":"ne",
         "New_Hampshire":"nh",
         "New_Jersey":"nj",
         "New_Mexico":"nm",
         "Nevada":"nv",
         "New_York":"ny",
         "Ohio":"oh",
         "Oklahoma":"ok",
         "Oregon":"or",
         "Pennsylvania":"pa",
         "Rhode_Island":"ri",
         "South_Carolina":"sc",
         "South_Dakota":"sd",
         "Tennessee":"tn",
         "Texas":"tx",
         "Utah":"ut",
         "Virginia":"va",
         "Vermont":"vt",
         "Washington":"wa",
         "Wisconsin":"wi",
         "West_Virginia":"wv",
         "Wyoming":"wy"
}

URL = "https://www.foodpantries.org/"

def get_dict():
    return DICT

# Function to clean the text, remove html tags and such
def clean_text(raw_text):
    clean_comp = re.compile("<.*?>")
    clean_text = re.sub(clean_comp, "", raw_text)
    clean_text = os.linesep.join([s for s in clean_text.splitlines() if s])
    clean_text = re.sub("\"", "", clean_text)
    clean_text = re.sub("'", "", clean_text)
    
    return clean_text

# Get the pantry website
def get_website(text):
    line = ""
    for l in text.split("\n"):
        if "View Website and Full Address" in l:
            line = l
    try:
        website = re.search("\"(.*)\" title", line).group(1)
        website = website.replace("\"", "")
        website = website.replace(" title", "").strip()
    except AttributeError:
        website = "invalid"
        
    return website

# Query the API
def query_pantries(sql_string, city, abrev, dest):
    try:
        url = URL + "ci/" + abrev + "-" + city
        data = urllib.request.urlopen(url)
        result = data.read().decode('utf-8')
        website = get_website(result)
        result = clean_text(result).split("\n")

        text = ""
        image = ""
        pantry = ""
        location = ""
        keep = False
        info = False
        pantry_finished = False
        times = 0

        print("city is", city)

        # Obtain the info we want
        for line in result:
            if "LocalBusiness" in line:
                keep = True
                pantry_finished = False
            if "View Website and Full Address" in line:
                keep = False
                info = False
                pantry_finished = True
            if keep and "name:" in line:
                pantry = line.replace("name:","").strip()
                info = True
            elif keep and "image:" in line:
                image = line.replace(",image:", "").strip()
            elif keep and "streetAddress" in line:
                location = line.replace("streetAddress:", "").replace(".","").strip()
            elif keep and "addressRegion" in line:
                location+= " " +line.replace("addressRegion:", "").strip()
                location = location[:-1]
            elif info and line:
                text += line.strip() + "\n"

            if (pantry_finished):
                # Write the pantry info and image url
                text = text.replace("}","").replace("{","").strip()
                if (pantry != "" and text != ""):
                    output = sql_string + "'" + pantry + "', '" + text + "', '" \
                             + city + "', '" + abrev + "', '" + location + "', '" \
                             + website + "', '" + image + "');"
                    fi = open(dest, "a+")
                    fi.write(output)
                    fi.close()
                    
                pantry_finished = False
                text = ""
                times += 1
            if times > 9:
                return
                                
    except UnicodeDecodeError as e:
        print("could no read data for ", city)
    except OSError:
        print("OS Error?", city)


def main():
    cities = "./states/cities/"
    print('Scraping the pantries by city per state')
    files = [f for f in listdir(cities) if isfile(join(cities,f))]
    dest = "./DB/pantries.sql"
    sql_string = "INSERT INTO PANTRIES (Pantry, Info, City, Abrv, Location, Website, Image) VALUES ("

    # Loop over every state and city in the US in the foodpantry.org website
    for f in files:
        file_path = cities+f
        abrev = DICT.get(f.split("-")[0])

        print("states is", abrev)
                
        with open(file_path) as fp:
            city = fp.readline().strip()
                
            while city:
                query_pantries(sql_string, city, abrev, dest)
                city = fp.readline().strip()

if __name__ == "__main__":
    main()

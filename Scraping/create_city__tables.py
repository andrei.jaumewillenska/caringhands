# Python script to create the cities table
# Author Andrei Jaume

import os
from os import listdir
from os.path import isfile, join
from food_pantry_city import get_dict

def main():
    cities = "./states/cities/"
    files = [f for f in listdir(cities) if isfile(join(cities,f))]
    dest = "./DB/cities.sql"
    sql_string = "INSERT INTO CITIES (City, Abrv) VALUES ("
    state_dict = get_dict()
    
    for f in files:
        if f != ".gitkeep":
            state = f.split("-")[0]
            abrv = state_dict[state]
            file_path = cities+f

            with open(file_path) as fp:
                city = fp.readline().strip().replace("'", "")
            
                while city:
                    output = sql_string + "'" + city + "', '" + abrv + "');"
                    fi = open(dest, "a+")
                    fi.write(output)
                    fi.close()
                    city = fp.readline().strip().replace("'", "")
    
if __name__ == "__main__":
    main()

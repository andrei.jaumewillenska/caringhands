import unittest
import requests
import json

url = 'http://api.caringhands.me/'
models = ['states', 'volunteer', 'foodpantries']

states = ["Alaska",
        "Alabama",
        "Arkansas",
        "Arizona",
        "California",
        "Colorado",
        "Connecticut",
        "District of Columbia",
        "Delaware",
        "Florida",
        "Georgia",
        "Hawaii",
        "Iowa",
        "Idaho",
        "Illinois",
        "Indiana",
        "Kansas",
        "Kentucky",
        "Louisiana",
        "Massachusetts",
        "Maryland",
        "Maine",
        "Michigan",
        "Minnesota",
        "Missouri",
        "Mississippi",
        "Montana",
        "North Carolina",
        "North Dakota",
        "Nebraska",
        "New Hampshire",
        "New Jersey",
        "New Mexico",
        "Nevada",
        "New York",
        "Ohio",
        "Oklahoma",
        "Oregon",
        "Pennsylvania",
        "Rhode Island",
        "South Carolina",
        "South Dakota",
        "Tennessee",
        "Texas",
        "Utah",
        "Virginia",
        "Vermont",
        "Washington",
        "Wisconsin",
        "West Virginia",
        "Wyoming"
]
instances = iter(['Texas', '33', '28083'])
minilists = ['opportunities', 'foodpantries']
searches = ["ex", 'ca','di','null','7.7','West','Texas']
filters = [{"region": ["West"], "poverty": ["50"]}, {"state": ["Texas", "California"]}]
sorts=[["alpha","poverty"],["alpha"],["ACS","DECS"]]

class MyTestMethods(unittest.TestCase):
	@classmethod
	def url_test(cls, key, source):
		cls.assertTrue(cls, key in source)
		cls.assertIsInstance(cls, source[key], str)
		cls.assertTrue(cls, source[key].startswith('http'))

	@classmethod
	def str_test(cls, key, source):
		cls.assertTrue(cls, key in source)
		cls.assertIsInstance(cls, source[key], str)

	@classmethod
	def float_test(cls, key, source):
		cls.assertTrue(cls, key in source)
		cls.assertIsInstance(cls, float(source[key]), float)
		cls.assertGreaterEqual(cls, float(source[key]), 0)

	@classmethod
	def list_test(cls, key, source):
		cls.assertTrue(cls, key in source)
		cls.assertIsInstance(cls, source[key], list)
		cls.assertGreaterEqual(cls, len(source[key]), 0)

	@classmethod
	def dict_test(cls, key, source):
		cls.assertGreaterEqual(cls, len(source), key)
		cls.assertIsInstance(cls, source[key], dict)
		cls.assertGreaterEqual(cls, len(source[key]), 1)

	@classmethod
	def int_test(cls, key, source):
		cls.assertTrue(cls, key in source)
		cls.assertIsInstance(cls, source[key], int)
		cls.assertGreaterEqual(cls, source[key], 0)

	@classmethod
	def listi_test(cls, source):
		cls.assertIsInstance(cls, source, list)
		cls.assertGreaterEqual(cls, len(source), 0)


class MyTest(MyTestMethods):
	def tearDown(self):
		print('---end test---')

	def setUp(self):
		print('\n')

	def test_list(self):
		print('---start test list---')
		for model in models:
			print('testing ' + model)
			r = requests.get(url + model)
			self.assertEqual(r.status_code, 200)
			d = json.loads(r.text)
			self.int_test('count', d)
			self.assertTrue('next' in d)
			self.assertTrue('previous' in d)
			self.list_test('results', d)
			self.dict_test(0, d['results'])

	def test_visualization(self):
		print('---start test visualization---')
		print('testing ' + "allstates")
		r = requests.get(url + "allstates")
		self.assertEqual(r.status_code, 200)
		d = json.loads(r.text)
		self.listi_test(d)
		self.dict_test(0, d)

		print('testing ' + "visualization")
		r = requests.get(url + "visualization")
		self.assertEqual(r.status_code, 200)
		d = json.loads(r.text)
		self.listi_test(d)
		self.dict_test(0, d)

	def test_minilist(self):
		print('---start test minilist---')
		for minilist in minilists:
			for state in states:
				print('testing ' + minilist + " in " + state)
				r = requests.get(url + "states/" + state + "/" + minilist)
				self.assertEqual(r.status_code, 200)
				d = json.loads(r.text)
				self.listi_test(d)
				if len(d)>0:
					self.dict_test(0, d)

	def test_search(self):
		print('---start test search---')
		for model in models:
			for search in searches:
				print('testing search ' + search + " in " + model)
				r = requests.get(url + model + "/?search=" + search)
				self.assertEqual(r.status_code, 200)
				d = json.loads(r.text)
				self.int_test('count', d)
				self.assertTrue('next' in d)
				self.assertTrue('previous' in d)
				self.list_test('results', d)
				if len(d['results'])>0:
					self.dict_test(0, d['results'])

	def test_filter(self):
		print('---start test filter---')
		for model in models:
			filter = filters[1]
			if model == 'states':
				filter = filters[0]
			for colunm in filter:
				for value in filter[colunm]:
					print('testing '+model+' filter for ' + colunm + " in " + value)
					r = requests.get(url + model + "/?" + colunm + "=" + value)
					self.assertEqual(r.status_code, 200)
					d = json.loads(r.text)
					self.int_test('count', d)
					self.assertTrue('next' in d)
					self.assertTrue('previous' in d)
					self.list_test('results', d)
					self.dict_test(0, d['results'])

	def test_sort(self):
		print('---start test sort---')
		for model in models:
			sort = sorts[1]
			if model == 'states':
				sort = sorts[0]
			for data in sort:
				for order in sorts[2]:
					print('testing '+ model+' sort '+ data+ " in " + order)
					r = requests.get(url + model + "/?sort=" + data + order)
					self.assertEqual(r.status_code, 200)
					d = json.loads(r.text)
					self.int_test('count', d)
					self.assertTrue('next' in d)
					self.assertTrue('previous' in d)
					self.list_test('results', d)
					self.dict_test(0, d['results'])


	def test_get(self):
		print('---start test get---')
		for model in models:
			print('testing ' + model, end=":\n")
			try:
				instance = next(instances)
			except BaseException:
				break
			r = requests.get(url + model + '/' + instance)
			self.assertEqual(r.status_code, 200)
			d = json.loads(r.text)
			self.str_test('name', d)
			print(d['name'])
			if model == 'states':
				self.float_test('poverty', d)
				self.str_test('description', d)
				self.url_test('pos_img', d)
				self.url_test('main_img', d)
			elif model == 'volunteer':
				self.int_test('id', d)
				self.str_test('description', d)
				self.list_test('state', d)
				self.str_test('address', d)
				self.url_test('website', d)
				self.url_test('main_img', d)
				self.url_test('state_pos_img', d)
				self.url_test('logo_img', d)
			else:
				self.int_test('id', d)
				self.list_test('state', d)
				self.url_test('state_pos_img', d)
				self.str_test('description', d)
				self.str_test('address', d)
				self.url_test('logo_img', d)
				self.url_test('website', d)


if __name__ == '__main__':
	unittest.main()

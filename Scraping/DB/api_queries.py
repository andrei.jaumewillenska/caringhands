# API Query for ISSUE #36 to obtain the State info
# Given in python state string they want to query

state = state.replace(" ", "_")
query = "SELECT * FROM caringhands_state WHERE name = '" + state + "';"

# API Query for ISSUES #39 and #40 to obtain a specific volunteering or food pantry instance info
# Given in python name string they want to query
# the api call should be something like GET caringhands.me/api/volunteering/?name='somename'

query_pantry = "SELECT * FROM caringhands_patries WHERE Pantry = '" + name + "';"
query_volunteering = "SELECT * FROM caringhands_volunteering WHERE volunteer = '"+ name + "';"

# API call for Issue #37
# given a state e.g state = Texas
# return food opporunities or pantries in Texas

state = state.replace(' ', "_")

# For a given state we can query pantries and volunteering opporunities in the state to display in cart
state = "Texas"; # for example
query_pantries = "SELECT caringhands_pantries.pantry, caringhands_pantries.image FROM caringhands_states INNER JOIN caringhands_pantries ON caringhands_states.Abrv = caringhands_pantries.Abrv AND caringhands_states.name = '" + state + "';"
query_volunteering = "SELECT caringhands_volunteering.volunteer, caringhands_volunteering.image FROM caringhands_states INNER JOIN caringhands_volunteering ON caringhands_states.Abrv = caringhands_volunteering.Abrv AND caringhands_states.name = '" + state + "';"


# API call for counting instances in a table
query = "SELECT COUNT(*) FROM caringhands_table;"

# To get the pantry images for pagination
query_pantry_images = "SELECT * FROM caringhands_pantry_images;"


# How to obtain a pantry in the same state as a volunteering opportunity
volunteer = #volunteering opportunity name
city = #city 
query = "SELECT caringhands_pantries.name, caringhands_pantries.logo_img FROM caringhands_volunteering INNER JOIN caringhands_pantries ON caringhands_volunteering.Abrv = caringhands_pantries.Abrv AND caringhands_volunteering.volunteer = '" + volunteer + "' AND caringhands_volunteering.city = '" + city +"';"

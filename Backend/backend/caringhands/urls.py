# -*- coding: UTF-8 -*-

from django.urls import path, include
from rest_framework.urlpatterns import format_suffix_patterns
from rest_framework.routers import DefaultRouter
from . import views
from caringhands.views import StatesViewSet, PantriesViewSet, CitiesViewSet, MiniPantriesViewSet, VolunteeringViewSet, \
    MiniVolunteeringViewSet, AllStatesViewSet
from rest_framework import renderers

states_list = StatesViewSet.as_view({
    'get': 'results',
    'post': 'create'
})
states_detail = StatesViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
cities_list = CitiesViewSet.as_view({
    'get': 'list',
    'post': 'create'
})
cities_detail = CitiesViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})
pantries_list = PantriesViewSet.as_view({
    'get': 'results',
    'post': 'create'
})
pantries_detail = PantriesViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

volunteering_list = VolunteeringViewSet.as_view({
    'get': 'results',
    'post': 'create'
})
volunteering_detail = VolunteeringViewSet.as_view({
    'get': 'retrieve',
    'put': 'update',
    'patch': 'partial_update',
    'delete': 'destroy'
})

pantries_mini = MiniPantriesViewSet.as_view({
    'get': 'get_mini'
})

volunteering_mini = MiniVolunteeringViewSet.as_view({
    'get': 'get_mini'
})

router = DefaultRouter()
router.register(r'states', views.StatesViewSet)
router.register(r'allstates', views.AllStatesViewSet)
router.register(r'visualization', views.VisualizationViewSet)
# router.register(r'api/cities', views.CitiesViewSet)
router.register(r'foodpantries', views.PantriesViewSet)
router.register(r'volunteer', views.VolunteeringViewSet)

urlpatterns = [
    path(r'states/', states_list),
    path(r'foodpantries/', pantries_list),
    path(r'volunteer/', volunteering_list),
    path(r'states/<str:state>/foodpantries/', pantries_mini),
    path(r'states/<str:state>/opportunities/', volunteering_mini),

    path('', include(router.urls)),
]

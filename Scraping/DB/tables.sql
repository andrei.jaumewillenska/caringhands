CREATE TABLE IF NOT EXISTS caringhands_states (
	Name varchar NOT NULL,
	Abrv varchar(2) UNIQUE,
	Description varchar,
	Flag_img varchar,
	Main_img varchar,
	Pos_img varchar,
	Poverty varchar
);

CREATE TABLE IF NOT EXISTS caringhands_cities (
	city varchar NOT NULL,
	abrv varchar(2) NOT NULL,
	Constraint city_state PRIMARY KEY (City, Abrv),
	FOREIGN KEY (Abrv) REFERENCES caringhands_states(Abrv)
);

CREATE TABLE IF NOT EXISTS caringhands_pantries (
       	Name varchar NOT NULL,
	Description varchar,
	City varchar NOT NULL,
	Abrv varchar NOT NULL,
	Address varchar NOT NULL,
	Website varchar,
	logo_img varchar,
	id int,
	FOREIGN KEY (City, Abrv) REFERENCES caringhands_cities(City, Abrv)
);

CREATE TABLE IF NOT EXISTS caringhands_volunteering (
	Name varchar NOT NULL,
	Description varchar,
	City varchar NOT NULL,
	Abrv varchar NOT NULL,
	Address varchar NOT NULL,
	Website varchar,
	logo_img varchar,
	state_pos_img varchar,
	main_img varchar,
	id int,
	FOREIGN KEY (City, Abrv) REFERENCES caringhands_cities(City, Abrv)
);

CREATE TABLE IF NOT EXISTS caringhands_pantry_images (
	Image varchar NOT NULL
);

#!/bin/bash

WEBSITE="https://www.foodpantries.org/"
FILE="tmp.txt"
STATES="./states/states.txt"
CITIES="cities.txt"

MODE="$1"

if [ $MODE = 'states' ]; then
    # Simple bash tool to obtain a list of all the states in the USA
    
    echo 'obtaining all the states in the us'
    
    curl -s -X GET $WEBSITE | grep "<td><a href=\""$WEBSITE"st/" > $FILE

    while read -r line;
    do
	ln=$(echo $line | cut -d ">" -f3)
	ln=$(echo $ln | rev)
	ln=$(echo $ln | cut -d "<" -f2)
	ln=$(echo $ln | rev)
	echo $ln | tr " " "_"
    done < $FILE > $STATES
    rm $FILE

elif [ $MODE = 'cities' ]; then
    #Simple bash tool to obtain all the cities in the states
    
    echo 'obtaining the cities for each state'

    while read -r line;
    do
	state=$(echo $line)
	curl -s -X GET $WEBSITE"/st/"$state | grep "<td><a href=\""$WEBSITE"ci/" > $FILE

	city_file=$state"-"$CITIES
	while read -r line;
	do
    	    ln=$(echo $line | cut -d ">" -f3)
    	    ln=$(echo $ln | rev)
    	    ln=$(echo $ln | cut -d "<" -f2)
    	    ln=$(echo $ln | rev)
    	    echo $ln | tr " " "_"

	done < $FILE > $city_file
	mv $city_file "./states/cities/"$city_file
	rm $FILE
    
    done < $STATES

elif [ $MODE = 'pantries' ]; then
    # Tool to scrap the data for the food pantries
    scraper='./food_pantry_city.py'
    
    echo 'scraping foodpantries.org for all the food pantries'
    python3 $scraper

else
    echo 'incorrect usage, must provide a cmd arg (states/cities/pantries) for the mode desired'
    exit    
fi

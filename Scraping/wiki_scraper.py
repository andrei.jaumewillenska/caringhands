# Author Andrei Jaume
# File to scrape wikipedia pages for the staes info needed for the project
# It will auto generate the States Table data
# This code creates the sql comamnds to run in PGAdmin and upload to the DB

import requests
import re
import os
import food_pantry_city

# Create an api session on the wiki endpoint
SESSION = requests.Session()
URL = "https://en.wikipedia.org/w/api.php"

def main():
    dest = "./DB/states.sql"
    params = {}
    states_file = "./states/states.txt"
    state_dict = food_pantry_city.get_dict()
    sql_string = "INSERT INTO STATES (Name, Abrv, Description, flag_img, pos_img) VALUES ("
    

    # Iterate over all the US states and query wikipedia
    with open(states_file) as fp:
        line = fp.readline().strip()
        
        while line:
            state = line
            # Fix erros on search
            if line == "District_of_Columbia":
                state = "Washington,_D.C."
            elif line == "Georgia":
                state = "Georgia_(U.S._state)"
            elif line == "New_York":
                state = "New_York_(state)"
            elif line == "Washington":
                state = "Washington_(state)"

            print(state)
            
            params = {
                "action": "parse",
                "page": state,
                "format": "json"
            }
            
            abrv = state_dict[line]
            # Append in batches in case anything fails work is saved
            text, flag_image = parse_page(params, state)
            pos_image = get_pos_image_url(params)
            
            output = sql_string + "'" + line.replace("_", " ") + "', '" + abrv +\
                     "', '" + text + "', '" + flag_image + "', '" + pos_image + "');"

            output = "UPDATE caringhands_states SET Description = '" + text + \
                     "' WHERE Abrv = '" + abrv + "';"

            f = open(dest, "a+")
            f.write(output)
            f.close()
            line = fp.readline().strip()
            
# Function to get the url for the wikipedia google state image
def get_pos_image_url(params):
    result = SESSION.get(url=URL, params=params)
    data = result.json()

    raw_text = data["parse"]["text"]["*"]
    lines = raw_text.split("\n")
    image = ""

    for l in lines:
        if "in_United_States" in l or "Location of Washington, D.C.," in l:
            image = re.search("href=\"/wiki/(.*)\" class=",l).group(1)
            break

    url = requests.get(
        "http://en.wikipedia.org/w/api.php?action=query&titles="+
        image+"+&prop=imageinfo&iiprop=url&format=json").json()
    
    try:
        image_url = url["query"]["pages"]["-1"]["imageinfo"][0]["url"]
        print("pos_img", image_url)
        return image_url
    except KeyError as e:
        print("Error in ", state, e)
        pass

# Function to parse the wikipedia html text
def parse_page(params, state):
    result = SESSION.get(url=URL, params=params)
    data = result.json()

    flag_image = get_image_url(state)
        
    # sections_list = data["parse"]["sections"]
    # sections = []
    # all_sections = []


    # # Obtain the sections we care about
    # for sect in sections_list:
    #     anchor = sect["anchor"]
    #     level = sect["level"]

    #     if level == "2":
    #         all_sections += [anchor]
            
    #     anchor_l = anchor.lower()
        
    #     if "etymology" in anchor_l or "health" in anchor_l or \
    #        "demographics" in anchor_l:
    #         sections += [anchor]

    raw_text = data["parse"]["text"]["*"]
    raw_text = clean_text(raw_text)
    lines = raw_text.split("\n")

    # Extract the text
    # keep = True
    # text = ""
    # for line in lines:
    #     if "Contents" in line:
    #         keep = False
    #     elif "[edit]" in line:
    #         sect = line.replace("[edit]","").strip().replace(" ", "_")
    #         if sect in all_sections:
    #             keep = sect in sections
    #     if keep:
    #         text += line.replace("[edit]","") + "\n"

    # return (text, image)

    text = ""
    keep = False

    for l in lines:
        if "Contents" in l:
            break
        if keep:
            text += l.replace("(listen","")+ "\n"
        if "Lists of United States state symbols" in l:
            keep = True
            
    return (text, flag_image)
    

# Function to clean up the html tags and text
def clean_text(raw_text):
    clean_comp = re.compile("<.*?>")
    clean_text = re.sub(clean_comp, "", raw_text)
    clean_text = clean_text.replace("\n", " \n")
    clean_text = re.sub("&#\d\d", "", clean_text)
    clean_text = re.sub(";\d\d\d;", "", clean_text)
    clean_text = re.sub("0;", "", clean_text)
    clean_text = re.sub("•0;", "", clean_text)
    clean_text = os.linesep.join([s for s in clean_text.splitlines() if s])
    clean_text = re.sub("\"", "", clean_text)
    clean_text = re.sub("'", "", clean_text)
    clean_text = re.sub(";\d;", "", clean_text)
    clean_text = re.sub(";\d\d;", "", clean_text)
    clean_text = re.sub(";\d", "", clean_text)
    return clean_text

# Function to get the image url
def get_image_url(state):
    result = requests.get(
        'http://en.wikipedia.org/w/api.php?action=query&titles='
        +state+'&prop=pageimages&format=json&pithumbsize=100')
    data = result.json()
    data = data['query']['pages']
    
    try:
        for key in data:
            image = data[key]["thumbnail"]["source"]
    except KeyError:
        print("couldn't get image url for %s " %state)
        image = "invalid"
        
    print("flag_img", image)
    return image

if __name__ == "__main__":
    main()

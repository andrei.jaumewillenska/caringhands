from django.shortcuts import render

from caringhands.models import States, Pantries, Cities, Volunteering
from caringhands.serializers import StatesSerializer, CitiesSerializer, PantriesSerializer, MiniPantriesSerializer, VolunteeringSerializer, MiniVolunteeringSerializer, VisualizationSerializer
from caringhands.Pagination import PaginationMixin
from rest_framework.decorators import action
from rest_framework import viewsets
from rest_framework.pagination import PageNumberPagination
from rest_framework.response import Response
from rest_framework import generics
from django.core.paginator import Paginator

class AllStatesViewSet(viewsets.ModelViewSet):
    queryset = States.objects.all()
    serializer_class = StatesSerializer
    model = States
    lookup_field = 'name'

class VisualizationViewSet(viewsets.ModelViewSet):
    queryset = States.objects.all()
    serializer_class = VisualizationSerializer
    model = States
    lookup_field = 'name'

class StatesViewSet(viewsets.ModelViewSet):
    queryset = States.objects.all()
    pagination_class = PageNumberPagination
    pagination_class.page_size = 16
    serializer_class = StatesSerializer
    model = States
    lookup_field = 'name'

    def results(self,request):
        search = request.GET.get('search','')
        search=search.lower()
        print("search:"+search)
        ins = "SELECT * FROM caringhands_states where (LOWER(caringhands_states.name) like '%%"+search+"%%' or cast(caringhands_states.poverty as text) like '%%"+search+"%%' or caringhands_states.region like '%%"+search+"%%' or caringhands_states.abrv like '%%"+search+"%%') "

        region = request.GET.get('region','')
        if region:
            ins += "and caringhands_states.region = '"+region+"'"
        poverty = request.GET.get('poverty','')
        if poverty:
            ins += "and caringhands_states.percentile = '"+poverty+"'"
        print("filter:" + region + " and " + poverty)
        sort = request.GET.get('sort','')
        if sort:
            print("sort "+ sort)
            if sort.startswith("alpha"):
                if sort[5]=='A':
                    ins += "order by name asc"
                else:
                    ins += "order by name desc"
            else:
                if sort[7]=='A':
                    ins += "order by poverty asc"
                else:
                    ins += "order by poverty desc"


        ins += ";"


        self.queryset = States.objects.raw(ins)
        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class CitiesViewSet(viewsets.ModelViewSet):
    """
    This viewset automatically provides `list`, `create`, `retrieve`,
    `update` and `destroy` actions.

    Additionally we also provide an extra `highlight` action.
    """
    queryset = Cities.objects.all()
    pagination_class = PageNumberPagination
    pagination_class.page_size = 16
    model=Cities
    serializer_class = CitiesSerializer
    lookup_field = 'city'

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class PantriesViewSet(viewsets.ModelViewSet):
    queryset = Pantries.objects.all()
    pagination_class = PageNumberPagination
    pagination_class.page_size = 16
    model = Pantries
    serializer_class = PantriesSerializer
    lookup_field = 'id'

    def results(self,request):
        ins = "SELECT * FROM caringhands_pantries "
        search = request.GET.get('search','')
        if search:
            search=search.lower()
            print("search:"+search)
            ins += "inner join caringhands_states using(abrv) where (LOWER(caringhands_pantries.name) like '%%"+search+"%%' or LOWER(caringhands_pantries.city) like '%%"+search+"%%' or LOWER(caringhands_states.name) like '%%"+search+"%%' or cast(caringhands_states.poverty as text) like '%%"+search+"%%')  "

        state = request.GET.get('state','')
        if state:
            if search:
                ins += "and "
            else:
                ins += "inner join caringhands_states using(abrv) where "
            print("state:" + state)
            ins += "caringhands_states.name = '"+state+"' "

        sort = request.GET.get('sort','')
        if sort:
            print("sort: "+ sort)
            if sort[5]=='A':
                ins += "order by caringhands_pantries.name asc"
            else:
                ins += "order by caringhands_pantries.name desc"

        ins += ";"

        self.queryset = Pantries.objects.raw(ins)
        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class VolunteeringViewSet(viewsets.ModelViewSet):
    queryset = Volunteering.objects.all()
    pagination_class = PageNumberPagination
    pagination_class.page_size = 16
    model = Volunteering
    serializer_class = VolunteeringSerializer
    lookup_field = 'id'


    def results(self,request):
        ins = "SELECT * FROM caringhands_volunteering "
        search = request.GET.get('search','')
        if search:
            search=search.lower()
            print("search:"+search)
            ins += "inner join caringhands_states using(abrv) where (LOWER(caringhands_volunteering.name) like '%%"+search+"%%' or LOWER(caringhands_volunteering.city) like '%%"+search+"%%' or LOWER(caringhands_states.name) like '%%"+search+"%%' or cast(caringhands_states.poverty as text) like '%%"+search+"%%')  "

        state = request.GET.get('state','')
        if state:
            print("state:" + state)
            if search:
                ins += "and "
            else:
                ins += "inner join caringhands_states using(abrv) where "
            ins += "caringhands_states.name = '"+state+"' and caringhands_states.abrv = caringhands_volunteering.abrv "

        sort = request.GET.get('sort','')
        if sort:
            print("sort "+ sort)
            if sort[5]=='A':
                ins += "order by caringhands_volunteering.name asc"
            else:
                ins += "order by caringhands_volunteering.name desc"

        ins += ";"

        self.queryset = Volunteering.objects.raw(ins)
        page = self.paginate_queryset(self.queryset)
        if page is not None:
            serializer = self.serializer_class(page, many=True)
            return self.get_paginated_response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class MiniPantriesViewSet(viewsets.ModelViewSet):
    model = Pantries
    queryset = Pantries.objects.all()
    max_instance = 5

    def get_mini(self, request, state):
        queryset = Pantries.objects.raw(
            "SELECT * FROM caringhands_pantries INNER JOIN caringhands_states ON caringhands_states.Abrv = caringhands_pantries.Abrv AND caringhands_states.name = '" +
            state +
            "';")
        queryset = queryset[:self.max_instance]
        serializer = MiniPantriesSerializer(queryset, many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)


class MiniVolunteeringViewSet(viewsets.ModelViewSet):
    model = Volunteering
    queryset = Volunteering.objects.all()
    max_instance = 5

    def get_mini(self, request, state):
        queryset = Volunteering.objects.raw(
            "SELECT * FROM caringhands_volunteering INNER JOIN caringhands_states ON caringhands_states.Abrv = caringhands_volunteering.Abrv AND caringhands_states.name = '" +
            state +
            "';")
        queryset = queryset[:self.max_instance]
        serializer = MiniVolunteeringSerializer(queryset, many=True)
        return Response(serializer.data)

    def perform_create(self, serializer):
        serializer.save(owner=self.request.user)
